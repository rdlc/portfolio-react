import React from 'react';
import Header from './components/Header/index'
import About from './components/About/index'

function App() {
  return (
    <div className="App">
      <Header />
      <About />
    </div>
  );
}

export default App;
