import React from "react";
import "./style.css";

function Header() {
  return (
    <>
      <div className="header">
        <ul>
          <li>
            <a className="" href="/">
              Home
            </a>
          </li>
          <li>
            <a className="" href="/">
              About
            </a>
          </li>
          <li>
            <a className="" href="/">
              Resume
            </a>
          </li>
          <li>
            <a className="" href="/">
              Works
            </a>
          </li>
          <li>
            <a className="" href="/">
              Contact
            </a>
          </li>
        </ul>
      </div>

      <div className="banner-background">
        <div className="banner">
          <h1>I'm Renzo De La Cruz</h1>
          <p>
            I'm a software developer, analist and webdesigner creating awesome
            and effective software products for companies of all sizes around
            the globe. Let's start scrolling and learn more about me.
          </p>
        </div>
      </div>
    </>
  );
}

export default Header;
